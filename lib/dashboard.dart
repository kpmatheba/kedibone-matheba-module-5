import 'package:appb/addbar.dart';
import 'package:flutter/material.dart';
import 'package:appb/login.dart';

class dashboard extends StatelessWidget {
  const dashboard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("dashboard"),
      ),
      body: ElevatedButton(
        onPressed: () => {},
        child: const Text("Search"),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => addbar()));
        },
        label: const Text('next'),
        icon: const Icon(Icons.navigate_next),
        backgroundColor: Colors.blue,
      ),
    );
  }
}
