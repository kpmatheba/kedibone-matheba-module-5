import 'package:appb/login.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'firebase_options.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Deli-Cious Bar',
      home: const login(),
      theme: ThemeData(
          primarySwatch: Colors.orange,
          accentColor: Colors.blue,
          scaffoldBackgroundColor: Colors.grey),
    );
  }
}
