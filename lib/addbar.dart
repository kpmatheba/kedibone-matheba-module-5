import 'dart:developer';
import 'dart:html';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class addbar extends StatefulWidget {
  const addbar({Key? key}) : super(key: key);

  @override
  State<addbar> createState() => _addbarState();
}

class _addbarState extends State<addbar> {
  @override
  Widget build(BuildContext context) {
    TextEditingController subjectController = TextEditingController();
    TextEditingController locationController = TextEditingController();

    Future _addbar() {
      final subject = subjectController.text;
      final location = locationController.text;

      final ref = FirebaseFirestore.instance.collection("Bar Type").doc();

      return ref
          .set({"Bar-Type": subject, "How_Many": Location, "doc_id": ref.id})
          .then((value) => log("collection added!!"))
          .catchError((onError) => log(onError));
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text('Search'),
        centerTitle: true,
      ),
      body: Column(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
            child: TextField(
              controller: subjectController,
              decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  hintText: "Enter your location"),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
            child: TextField(
              controller: locationController,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                hintText: "Select Date",
              ),
            ),
          )
        ],
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => addbar()));
        },
        label: const Text('next'),
        icon: const Icon(Icons.navigate_next),
        backgroundColor: Colors.blue,
      ),
    );
  }
}
